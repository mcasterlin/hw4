# ---------------------------------------------------- #
# File: hw4.py
# ---------------------------------------------------- #
# Author(s): Michael Casterlin & Nita Soni, Mcasterlin
# ---------------------------------------------------- #
# Plaftorm:    {Windows 7}
# Environment: Python 2.7.8
# Libaries:    numpy 1.9.0
#              matplotlib 1.4.0
#              scipy 0.14.0-Py2.7
#              Tkinter
# ---------------------------------------------------- #
# Keys:
# Player 1: up   - 'w'
#           down - 's'
# Player 2: up   - '<Up>'
#           down - '<Down>'
# ---------------------------------------------------- #
# TODO:
# - 
# ---------------------------------------------------- #
'''
#Psuedocode/Outline
import tKinter, numpy, random, tkFont
class GUI(Frame):
    __init__: create grid, create Pong game, bind keys, create widgets, create field space
    bindkeys: control vertical paddle movements
    createWidgets: make New Game reset button, make quit button
    draw: draw all elements at current iteration onto canvas
class Paddle(object):
    __init__: height, upbutton, downbutton
    lower: move paddle down
    lift: move paddle up
class Ball(object):
    __init__: position(x,y), speed, velocity(x,y), size
    reflect: physics of bounce ball off object [scaled dot product of surface vector and velocity vector]
    hitPaddle: calls reflect and increments number of hits tally; if tally>=5, increase ball speed
    move: update position by adding velocity to current position
    strike/reset: Handles scoring and New Game; updates score, resets ball parameters
class Pong(object):
    __init__: create paddles, create ball, define game events and responses
        ball hitting upper and lower walls, left and right walls (scoring), and paddles
    step: update position/velocities/properties of each item in game every n seconds concurrent with mainloop
class Score(object):
    __init__ score keeping object
    update: increment appropriate player's score
'''

import Tkinter as tk
from numpy import sin, pi, cos
import random
import tkFont

class GUI(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
 
        # Set up the grid space for the application
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.pong = Pong()

        self.bindKeys(master)
        self.createWidgets()
        self.pack()

        self.field = tk.Canvas(master, width=1000, height=500)

    # keybindings to control the positioning of the paddles
    def bindKeys(self, master):
        master.bind("w", self.pong.paddle_left.lift)
        master.bind("s", self.pong.paddle_left.lower)
        master.bind("<Up>", self.pong.paddle_right.lift)
        master.bind("<Down>", self.pong.paddle_right.lower)

    # creates buttons to exit the game or reset the field conditions
    def createWidgets(self):
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=1,row=0)
        self.play = tk.Button(self, text="New Game", command=self.pong.ball.reset).grid(column=0,row=0)

    # draw the updated boundaries of all relevant game elements (paddles, ball, etc) on the canvas game field
    def draw(self):
        app.field.delete("all")
        #Create paddles
        self.field.create_rectangle(20, self.pong.paddle_left.height, 50, (self.pong.paddle_left.height-100), fill="red")
        self.field.create_rectangle(950, self.pong.paddle_right.height, 980, (self.pong.paddle_right.height-100), fill="blue")
        #Display score
        ourFont = tkFont.Font(family='Helvetica',size=20)
        app.field.create_text(250, 25, text=str(score.scoreP1), font=ourFont)
        app.field.create_text(750, 25, text=str(score.scoreP2), font=ourFont)
        #Create field details
        self.field.create_line(500, 0, 500, 500, fill="#476042", width=3)
        self.field.create_oval(400,150,600,350, width=3)
        #Create ball
        self.field.create_oval(self.pong.ball.position[0]-self.pong.ball.size, 
            self.pong.ball.position[1]-self.pong.ball.size, 
            self.pong.ball.position[0]+self.pong.ball.size, 
            self.pong.ball.position[1]+self.pong.ball.size,
            fill="black")
        self.field.pack()

#Class for creatting the two paddle objects use to interact with the game
class Paddle(object):
    def __init__(self, position, upbutton, downbutton):
        """
        Create a pong paddle with the given position and keybindings
        """
        self.height = position
        self.KeyUp = upbutton
        self.KeyDown = downbutton

    def lower(self,event):
        """
        Given appropriate keypress, move paddle down.
        """
        if self.height <= 500:
            self.height += 10
        app.draw()

    def lift(self,event):
        """
        Given appropriate keypress, move paddle up.
        """
        if self.height >= 100:
            self.height -= 10
        app.draw()   

#Class that defines the properties of the game ball
class Ball(object):
    def __init__(self, position, size, speed):
        """
        Create a pong ball with the given position, size, and speed. 
        """
        self.initPosition = position 
        self.initSpeed = speed
        self.velocity = (0,0)
        self.size = size # Radius
        self.player = 0 # For score/new game reset

        self.reset()

    def reflect(self, surface):
        """
        Alter the ball's velocity for a perfectly elastic
        collision with a surface defined by the unit normal surface.
        """
        diagonal = -2 * dot(surface, self.velocity)
        self.velocity = add(self.velocity, scale(surface, diagonal))

    def hitPaddle(self, surface):
        """
        Manage collision of ball and paddle. Reflect ball and increase
        ball's speed every 5 hits.
        """
        self.reflect(surface)
        self.hitTally+=1
        if self.hitTally>=5:
            self.speed+=1
            self.velocity=(self.velocity[0]*self.speed/(self.speed-1),self.velocity[1]*self.speed/(self.speed-1))
            self.hitTally=0

    def move(self):
        """
        Increment ball position, assuming no collisions.
        """
        self.position = add(self.position, self.velocity)

    def strike(self, player):
        """
        Set player to update score.
        """
        self.player = player
        self.reset()

    def reset(self):
        """
        Handle game reset in case of goal or New Game button. Update scores and reset ball and paddle parameters.
        """
        score.update(self.player)
        self.position = self.initPosition
        self.speed = self.initSpeed
        scalar = random.randrange(0,100)/100.
        angle = scalar*2*pi
        vx = self.speed*cos(angle)
        vy = self.speed*sin(angle)  
        self.velocity = (vx, vy)
        self.hitTally = 0
        self.player = 0

#Class that controls the interaction between game and field elements
class Pong(object):
    def __init__(self):
        """
        Create a pong game. Create standard pong objects, events, and
        responses.
        """
        self.paddle_left = Paddle(300,"w","s")
        self.paddle_right = Paddle(300,"<Up>","<Down>")
        self.ball = Ball((500,250), 10, 3)
        # Game events: Ball hits [upper wall, lower wall, left paddle, right paddle, left wall, right wall]
        self.events = [lambda: self.ball.position[1] > 10,
                       lambda: self.ball.position[1] < 490,
                       self.hits_left_paddle,
                       self.hits_right_paddle,
                       lambda: self.ball.position[0] > 995,
                       lambda: self.ball.position[0] < 5]
        # Game responses: [bounce back in appropriate direction x4, score ball x2]
        self.responses = [lambda: self.ball.reflect( (0, -1) ),
                          lambda: self.ball.reflect( (0,  1) ),
                          lambda: self.ball.hitPaddle( (1,  0) ),
                          lambda: self.ball.hitPaddle( (-1, 0) ),
                          lambda: self.ball.strike(1),
                          lambda: self.ball.strike(2)]

    # Pong event for ball collision with left paddle
    def hits_left_paddle(self):
        if self.ball.velocity[0]<0:
            return self.ball.position[0] < 50 and self.ball.position[1] <= self.paddle_left.height and self.ball.position[1] >= self.paddle_left.height-100
    
    # Pong event for ball collision with right paddle
    def hits_right_paddle(self):
        if self.ball.velocity[0]>0:
            return self.ball.position[0] > 950 and self.ball.position[1] <= self.paddle_right.height and self.ball.position[1] >= self.paddle_right.height-100

    def step(self):
        """
        Calculate the next game state.
        """
        self.ball.move()
        # Check for events
        for event, response in zip(self.events, self.responses):
            if event():
                response()
                app.draw() 
        # Run pong game with time step size of 0.005 seconds
        root.after(5, self.step)

#Class to create the object which stores information about game results
class Score(object):
    def __init__(self):
        """
        Create 2-player score keeping object.
        """
        self.scoreP1 = 0
        self.scoreP2 = 0

    def update(self, player):
        """
        Increment a player's score.
        """
        if player == 1:
            self.scoreP1 += 1
        elif player == 2:
            self.scoreP2 += 1
        elif player == 0:
            self.scoreP1 = 0
            self.scoreP2 = 0

def dot(x, y):
    """
    2D dot product
    """
    return x[0]*y[0] + x[1]*y[1]

def scale(x, a):
    """
    2D scalar multiplication
    """
    return (x[0]*a, x[1]*a)

def add(x, y):
    """
    2D vector addition
    """
    return (x[0]+y[0], x[1]+y[1])

root = tk.Tk()
score = Score()
app = GUI(master=root)
app.master.title("Chaos Pong")
app.pong.step()
app.mainloop()
root.destroy()